// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDS2.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS2, "TDS2" );

DEFINE_LOG_CATEGORY(LogTDS2)
 